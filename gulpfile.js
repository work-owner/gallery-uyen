var gulp = require('gulp');
var sass = require('gulp-sass');
var cssmin = require('gulp-cssmin');
var imagemin = require("gulp-imagemin");

gulp.task('imagemin', function () {
	return gulp.src('src/assets/images/*')
		.pipe(imagemin())
		.pipe(gulp.dest('build/assets/images'))
})
gulp.task('sassdev', () => {
	return gulp.src('src/assets/css/*.scss')
		.pipe(sass().on('error', sass.logError))
		.pipe(cssmin())
		.pipe(gulp.dest('dev/assets/css'))

});

gulp.task('jsbuild', () => {
	gulp.src('src/assets/js/*.js')
		.pipe(gulp.dest('build/assets/js'));
});
gulp.task('sass', () => {
	return gulp.src('src/assets/css/*.scss')
		.pipe(sass().on('error', sass.logError))
		// .pipe(cssmin())
		.pipe(gulp.dest('build/assets/css'))

});
gulp.task('template', function () {
	gulp.src('src/*.html')
		.pipe(gulp.dest('build'));
});

gulp.task('develop', gulp.series('sass','imagemin', 'sassdev','template', 'jsbuild'));
gulp.task('sass:watch', function () {
	gulp.watch('src/assets/*.scss', gulp.series(['sassdev']));
});
